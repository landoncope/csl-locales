# @manuscripts/csl-locales

The data in this repository is derived from the [CSL Locales Repository](https://github.com/citation-style-language/locales).

# Updating

To update locales run `git submodule update --remote` then `npm run build`.

# Licensing

All the locale files in this repository are released under the [Creative Commons Attribution-ShareAlike 3.0 Unported license](http://creativecommons.org/licenses/by-sa/3.0/). For attribution, any software using CSL locale files from this repository must include a clear mention of the CSL project and a link to [CitationStyles.org](http://citationstyles.org/). When distributing these locale files, the listings of translators in the locale metadata must be kept as is.
